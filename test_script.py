#!/usr/bin/env python

import unittest
import script

class TestScript(unittest.TestCase):
    def test_add(self):
        self.assertEquals(
            script.add(10, 5),
            15
        )
        self.assertEquals(
            script.add(-1, 1),
            0
        )
        self.assertEquals(
            script.add(0, 1),
            1
        )
        self.assertEquals(
            script.add(1, 0),
            1
        )


    def test_subtract(self):
        self.assertEquals(
            script.subtract(10, 5),
            5
        )
        self.assertEquals(
            script.subtract(1, 0),
            1
        )
        self.assertEquals(
            script.subtract(0, 5),
            -5
        )
        self.assertEquals(
            script.subtract(-10, 5),
            -15
        )


    def test_multiply(self):
        self.assertEquals(
            script.multiply(2, 3),
            6
        )
        self.assertEquals(
            script.multiply(2, 3),
            6
        )
        self.assertEquals(
            script.multiply(2, 3),
            6
        )
        self.assertEquals(
            script.multiply(2, 3),
            6
        )


    def test_divide(self):
        self.assertEquals(
            script.divide(10, 5),
            2
        )
        self.assertEquals(
            script.divide(5, 10),
            0.5
        )
        self.assertEquals(
            script.divide(0, 5),
            0
        )

        with self.assertRaises(ValueError):
            script.divide(10, 0)


if __name__ == '__main__':
    unittest.main()
