#!/usr/bin/env python

import logging
import sys

#######################################
### Logging Settings ##################
#######################################

logger = logging.getLogger()
logger.setLevel(logging.INFO)

#######################################
### Main Function #####################
#######################################

def main():
    x, y = arg_management(sys.argv[1:])

    sum         = add(x, y)
    difference  = subtract(x, y)
    product     = multiply(x, y)
    ratio       = divide(x, y)

    return [sum, difference, product, ratio]


#######################################
### Helper Functions ##################
#######################################

def show_help(exit_code=0, script_name=sys.argv[0]):
    help_message = '''
    Usage:
        {} <float> <float>
    '''.format(script_name)

    print(help_message)
    sys.exit(exit_code)


def arg_management(args_array):
    if len(args_array) < 2:
        logger.fatal('Not enough arguments supplied')
        sys.exit(1)
    else:
        try:
            x = float(args_array[0])
            y = float(args_array[1])
        except ValueError as e:
            logger.fatal('Expected a number to be passed in as an argument.')
            sys.exit(2)

    return x, y


#######################################
### Maths Functions ###################
#######################################

def add(x, y):
    sum = x + y
    logger.info('SUM: {}'.format(sum))

    return sum


def subtract(x, y):
    diff = x - y
    logger.info('Difference: {}'.format(diff))

    return diff


def multiply(x, y):
    product = x * y
    logger.info('Product: {}'.format(product))

    return product


def divide(x, y):
    if y == 0:
        raise ValueError
    else:
        ratio = float(x) / y
        logger.info('Ratio: {}'.format(ratio))

        return ratio


#######################################
### Execution #########################
#######################################

if __name__ == '__main__':
    stream = logging.StreamHandler(sys.stdout)

    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    stream.setFormatter(formatter)

    logger.addHandler(stream)

    main()

